package semestralniPrace.PR1.dusanjencik.gameoflife;

import java.awt.Point;
import java.io.InputStream;
import java.util.Scanner;
import java.util.Timer;

/**
 * Třída obstarávající hru
 * @author Dušan Jenčík
 */
public class Game {
    
    /**
     * Instance herního plátna
     */
    private Platno platno;
    
    /**
     * Instance herní smyčky
     */
    private GameLoop loop;
    
    /**
     * Instance timeru
     */
    private Timer timer;
    
    /**
     * Velikost herního pole zadaného souřadnicí
     */
    private Point field;
    
    /**
     * Pole jednotlivých buněk
     */
    private Pole pole = new Pole();
   
    /**
     * Konstruktor hry
     * @param dimensions souřadnice pro vykreslení herního pole
     * @param field velikost šachovnice
     */
    public Game(Point dimensions, Point field)
    {
        this.field = field;

        createField();
        loop = new GameLoop(this);
        platno = new Platno(dimensions, this, loop);
        
    }
    
    /**
     * Nastavení (vytvoření) buňky a jejího stavu
     * @param x X-souřadnice
     * @param y Y-souřadnice
     * @param stav logický stav (false = neživá)
     */
    public void setCell(int x, int y, boolean stav)
    {
        if(pole.cells[x][y] == null)
            pole.cells[x][y] = new Cell(stav);
        else
            pole.cells[x][y].setState(stav);
    }
    
    /**
     * Nastaví počet živých sousedů
     * @param x X-souřadnice
     * @param y Y-souřadnice
     * @param number počet živých sousedů
     */
    public void setCellNumberOfNeighbors(int x, int y, int number)
    {
        pole.cells[x][y].setNumberOfNeightbors(number);
    }
    
    /**
     * Nastavení Timeru a jeho spuštění v GameLoop
     */
    public void setTimer(boolean start)
    {
        if(start)
        {
            timer = new Timer();
            timer.schedule(loop = new GameLoop(this), 0, 100);
            platno.setLoop(loop);
            loop.setRunning(true);
        }
        else
        {
            timer.cancel();
            timer = null;
            loop.setRound(0);
        }
    }
    
    public void setPole(Pole pole)
    {
        this.pole = pole;
        loop.numberingNeighbors();
        if(loop.getRunning())
        {
            loop.setRunning(false);
            timer.cancel();
            timer = null;
            loop.setRound(1);
        }
        
        setTimer(true);
    }
    
    /**
     * Vrátí stav buňky
     * @param x souřadnice x
     * @param y souřadnice y
     * @return boolean stav buňky
     */
    public boolean getCellState(int x, int y)
    {
        return pole.cells[x][y].getState();
    }
    
    /**
     * Vrátí pole
     * @return pole buněk
     */
    public Cell[][] getCells()
    {
        return pole.cells;
    }
    
    /**
     * Vrátí Pole pro serializaci
     * @return Pole
     */
    public Pole getPole()
    {
        return pole;
    }
    
    /**
     * Vrátí počet sousedů
     * @param x souřadnice x
     * @param y souřadnice y
     * @return počet sousedů
     */
    public int getCellNumberOfNeighbors(int x, int y)
    {
        return pole.cells[x][y].getNumberOfNeightbors();
    }
    
    /**
     * Vrátí instanci plátna
     * @return instance plátna
     */
    public Platno getPlatno()
    {
        return platno;
    }
    
    public Point getField()
    {
        return field;
    }
    
    /**
     * Překreslí plátno
     */
    public void repaint()
    {
        platno.repaint();
    }
    
    /**
     * Inicializační funkce, která vytvoří herní smyčku a vykreslí ukázkové buňky
     */
    public void init()
    {
        String[] gosperGun = loadInternalFile("gosperGun.txt").split("\r\n");
        String[] pulsar = loadInternalFile("pulsar.txt").split("\r\n");
        
        for(int i = 0; i < gosperGun.length; i++)
        {
            String[] pos = gosperGun[i].split(" ");
            setCell(Integer.valueOf(pos[0])+2, Integer.valueOf(pos[1])+1, true);
        }
        
        for(int i = 0; i < pulsar.length; i++)
        {
            String[] pos = pulsar[i].split(" ");
            setCell(Integer.valueOf(pos[0])+6, Integer.valueOf(pos[1])+18, true);
        }
        loop.numberingNeighbors();
    }
    
    /**
     * Přečtení interního souboru
     * @param filename název souboru
     * @return obsah souboru
     */
    private String loadInternalFile(String filename)
    {
        InputStream s = Main.class.getClassLoader().getResourceAsStream(filename);
        Scanner sc = new Scanner(s);
        sc.useDelimiter("\\Z");
        return sc.next();
    }
    
    /**
     * Načtení polí ze Stringu
     * @param field Stringové pole
     */
    public void setField(String fields)
    {
        clear();
        String[] rows = fields.split("\n");
        for(int i = 0; i < field.x; i++)
        {
            String[] cell = rows[i].split("");
            for(int j = 0; j < field.y; j++)
            {
                setCell(i, j, cell[j].equals("1")?true:false);
            }
        }
        loop.numberingNeighbors();
        if(loop.getRunning())
        {
            loop.setRunning(false);
            timer.cancel();
            timer = null;
            loop.setRound(1);
        }
        
        setTimer(true);
    }
    
    /**
     * Generátor pole pro uložení
     * @return String, který se následně uloží do souboru
     */
    public String generateField()
    {
        String vystup = "";
        for(int i = 0; i < field.x; i++)
        {
            for(int j = 0; j < field.y; j++)
            {
                vystup += String.valueOf(pole.cells[i][j].getStateToString());
            }
            vystup += "\n";
        }
        return vystup;
    }
    
    /**
     * Vyčištění herního pole
     */
    public void clear()
    {
        for(int i = 0; i < field.x; i++)
        {
            for(int j = 0; j < field.y; j++)
            {
                setCell(i, j, false);
            }
        }
    }
    
    /**
     * Vytvoření pole
     */
    private void createField()
    {
        pole.cells = new Cell[field.x][field.y];
        
        for(int i = 0; i < field.x; i++)
        {
            for(int j = 0; j < field.y; j++)
            {
                setCell(i, j, false);
            }
        }
    }
}
