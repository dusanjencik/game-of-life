package semestralniPrace.PR1.dusanjencik.gameoflife;

/**
 * Třída (struktura) obstarávající reálnou souřadnici
 * @author Dušan Jenčík
 */
public class PointF {
    
    /**
     * Reálné hodnoty
     */
    public double x, y;
    
    /**
     * Konstruktor
     * @param x double parametr x souřadnice
     * @param y double parametr y souřadnice
     */
    public PointF(double x, double y)
    {
        this.x = x;
        this.y = y;
    }
}
