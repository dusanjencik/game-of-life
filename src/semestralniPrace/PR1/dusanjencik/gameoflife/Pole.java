package semestralniPrace.PR1.dusanjencik.gameoflife;

import java.io.Serializable;

/**
 * Třída uchovávající buňky
 * @author Dušan Jenčík
 */
public class Pole implements Serializable{
    /**
     * Pole jednotlivých buněk
     */
    public Cell[][] cells;
}
