package semestralniPrace.PR1.dusanjencik.gameoflife;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

/**
 * Třída pro vykreslování
 * @author Dušan Jenčík
 */
public class Draw {
    
    /**
     * Grafika
     */
    private Graphics2D g;
    
    /**
     * Rozměry herního pole
     */
    private Point dimensions;
    
    /**
     * Velikost šachovnice
     */
    private Point field;
    
    /**
     * Velikost jednoho pole
     */
    private PointF sizeOfField;
    
    /**
     * Konstruktor pomocné třídy pro vykreslování
     * @param g grafika
     * @param dimensions rozměry hracího pole
     * @param field velikost šachovnice
     * @param sizeOfField velikost jednoho pole
     * @param main instance hlavní třídy
     */
    public Draw(Graphics2D g, Point dimensions, Point field, PointF sizeOfField)
    {
        this.g = g;
        this.dimensions = dimensions;
        this.field = field;
        this.sizeOfField = sizeOfField;
    }
    
    /**
     * Vykreslí ohraničení Canvasu
     */
    public void border()
    {
        g.draw(new Line2D.Double(dimensions.x, dimensions.y, 
                dimensions.x, dimensions.y + field.y*sizeOfField.y));
        g.draw(new Line2D.Double(dimensions.x, dimensions.y, 
                dimensions.x + field.x*sizeOfField.x, dimensions.y));
        g.draw(new Line2D.Double(dimensions.x + field.x*sizeOfField.x, dimensions.y,
                dimensions.x + field.x*sizeOfField.x, dimensions.y + field.y*sizeOfField.y));
        g.draw(new Line2D.Double(dimensions.x, dimensions.y + field.y*sizeOfField.y,
                dimensions.x + field.x*sizeOfField.x, dimensions.y + field.y*sizeOfField.y));
    }
    
    /**
     * Pomocná funkce pro kreslení více řádků
     * @param text text k vykreslení
     * @param x souřadnice X
     * @param y souřadnice Y
     */
    public void drawString(String text, int x, int y) {
        for (String line : text.split("\n"))
        {
            String tab[] = line.split("\t");
            g.drawString(tab[0], x, y);
            if(tab.length > 1)
                g.drawString(tab[1], Main.guiSize.width - 150, y);
            y += g.getFontMetrics().getHeight();
        }
    }
    
    /**
     * Vykreslí manuál
     * @param g graphics
     * @param states stavy hodnot
     */
    public void manual(Object[] states)
    {
        drawString("quit\tesc\n"+
                "start game\tr | enter\n"+
                "save game\ts\n"+
                "open game\to",
                Main.guiSize.width - 260,
                50);
        
        if((boolean)states[1])
        {
            Color c = g.getColor();
            g.setColor(Color.WHITE);
            g.drawString("PAUSED", Main.guiSize.width - 200,
                125);
            g.setColor(c);
        }
        
        drawString("running\tr" + state((boolean)states[0]) +
                "\npause\tp" + state((boolean)states[1]) +
                "\nstep\tx[" + states[2] + "]" +
                "\nneighbours\tn" + state((boolean)states[3])
                , 
                Main.guiSize.width - 260,
                150);
        
    }
    
    /**
     * Pomocná funkce pro vykreslení stavu
     * @param state logická hodnota
     * @return řetězec
     */
    private String state(boolean state)
    {
        if(state)
            return "[*]";
        return "[ ]";
    }
    
    /**
     * Vykreslení copyrightu
     * @param g graphics
     */
    public void copyright()
    {
        g.drawString("© Copyright 2012 Dušan Jenčík", 
                Main.guiSize.width - 240,
                Main.guiSize.height - 40);
    }
    
    /**
     * Vykreslí nadpis hry
     * @param g graphics
     */
    public void gameOfLife()
    {
        g.drawString("GAME OF LIFE", 
                Main.guiSize.width - 240, 
                30);
    }
    
    /**
     * Vykreslí úvodní větu
     * @param g graphics
     */
    public void startSentece()
    {
        drawString("To start press\nEnter or R key", 
                40, 80);
    }

    /**
     * Vykreslení mřížky šachovnice
     */
    public void matrix()
    {
        Color c = g.getColor();
        g.setColor(new Color(30, 30, 30));

        for(int x = 0; x < field.x; x++)
        {
            g.draw(new Line2D.Double(dimensions.x + x*sizeOfField.x, dimensions.y,
                    dimensions.x + x*sizeOfField.x, dimensions.y + field.y*sizeOfField.y));
        }

        for(int y = 0; y < field.y; y++)
        {
            g.draw(new Line2D.Double(dimensions.x, dimensions.y + y*sizeOfField.y,
                    dimensions.x + field.x*sizeOfField.x, dimensions.y + y*sizeOfField.y));
        }
        g.setColor(c);
    }

    /**
     * Vykreslí živé buňky
     * @param cells seznam buňek
     */
    public void lives(Cell[][] cells, boolean showNeighbors)
    {
        for(int i = 0; i < field.x; i++)
        {
            for(int j = 0; j < field.y; j++)
            {
                if(cells[i][j].getState())
                {
                    Color c = g.getColor();
                    g.setColor(Color.WHITE);
                    g.fill(new Rectangle2D.Double(dimensions.x + i*sizeOfField.x, 
                            dimensions.y + j*sizeOfField.y, 
                           sizeOfField.x, sizeOfField.y));
                    g.setColor(c);
                    
                    if(showNeighbors)
                    {
                        g.setFont(g.getFont().deriveFont(7f));
                        c = g.getColor();
                        g.setColor(Color.BLACK);

                        g.drawString(cells[i][j].toString(), 
                                (float)(dimensions.x + i*sizeOfField.x + sizeOfField.x/3f), 
                                (float)(dimensions.y + j*sizeOfField.y + sizeOfField.y/1.2f));

                        g.setColor(c);
                    }
                }
                else
                {
                    if(showNeighbors)
                    {
                        g.setFont(g.getFont().deriveFont(7f));
                        g.drawString(cells[i][j].toString(), 
                                (float)(dimensions.x + i*sizeOfField.x + sizeOfField.x/3f), 
                                (float)(dimensions.y + j*sizeOfField.y + sizeOfField.y/1.2f));
                    }
                }
            }
        }
    }
}
