package semestralniPrace.PR1.dusanjencik.gameoflife;

import java.util.TimerTask;

/**
 * Třída hrací smyčky
 * @author Dušan Jenčík
 */
public class GameLoop extends TimerTask{

    /**
     * Logická stavová hodnota pauzy pro opakování aktualizace
     */
    private boolean pause = false;
    
    /**
     * Počet generací (kol) 
     */
    private int round = 0;
    
    /**
     * Logická stavová hodnota vypovídající o spuštění herní smyčky
     */
    private boolean running = false;
    
    /**
     * Předaná instance třídy Game
     */
    private Game game;
    
    /**
     * Inicializace herní smyčky
     * @param game 
     */
    public GameLoop(Game game)
    {
        this.game = game;
    }
    
    /**
     * Nastaví logickou hodnotu pro běh hrací smyčky
     * @param stav 
     */
    public void setRunning(boolean stav)
    {
        running = stav;
    }
    
    /**
     * Nastavení generace  
     * @param round číslo generace
     */
    public void setRound(int round)
    {
        this.round = round;
    }
    
    /**
     * Nastaví pauzu
     * @param pause pauza
     */
    public void setPause(boolean pause)
    {
        this.pause = pause;
    }
    
    /**
     * Vrátí logickou hodnotu o běhu programu
     * @return logická hodnota spuštěné herní smyčky
     */
    public boolean getRunning()
    {
        return running;
    }
    
    /**
     * Vrátí logickou hodnotu pauzy
     * @return pauza
     */
    public boolean getPause()
    {
        return pause;
    }
    
    /**
     * Vrátí počet generací
     * @return číslo informující o počtu generací
     */
    public int getRound()
    {
        return round;
    }
    
    /**
     * Hrací smyčka
     */
    @Override
    public void run() {
        if(!pause)
            step();
    }
    
    /**
     * Jeden krok (postup) volán z hrací smyčky
     */
    public void step()
    {
        round++;
        updateGame();
        numberingNeighbors();
        
        game.repaint();
    }
    
    /**
     * Očíslování polí počtem sousedů
     */
    public void numberingNeighbors()
    {
        for(int i = 0; i < game.getField().x; i++)
        {
            for(int j = 0; j < game.getField().y; j++)
            {
                // Okolí
                int okoli = 0;
                // vlevo
                okoli += isInArea(i-1, j-1);
                okoli += isInArea(i-1, j);
                okoli += isInArea(i-1, j+1);
                // střed
                okoli += isInArea(i, j-1);
                okoli += isInArea(i, j+1);
                // vpravo
                okoli += isInArea(i+1, j-1);
                okoli += isInArea(i+1, j);
                okoli += isInArea(i+1, j+1);
                
                game.setCellNumberOfNeighbors(i, j, okoli);
            }
        }
    }
    
    /**
     * Zjištění, jestli je daná buňka živá
     * @param x souřadnice x
     * @param y souřadnice y
     * @return 1 pokud je živá, 0 pokud není živá
     */
    private int isInArea(int x, int y)
    {
        if(x >= 0 && x < game.getField().x
                && y >= 0 && y < game.getField().y
                && game.getCellState(x, y))
            return 1;
        return 0;
    }
    
    /**
     * Znovuvyhodnocení celého hracího pole
     */
    private void updateGame()
    {
        for(int i = 0; i < game.getField().x; i++)
        {
            for(int j = 0; j < game.getField().y; j++)
            {
                if(game.getCellState(i, j))
                {
                    if(game.getCellNumberOfNeighbors(i, j) < 2)
                    {
                        game.setCell(i, j, false);
                    }
                    else if(game.getCellNumberOfNeighbors(i, j) == 2
                            || game.getCellNumberOfNeighbors(i, j) == 3)
                    {
                        game.setCell(i, j, true);
                    }
                    else if(game.getCellNumberOfNeighbors(i, j) > 3)
                    {
                        game.setCell(i, j, false);
                    }
                }
                else
                {
                    if(game.getCellNumberOfNeighbors(i, j) == 3)
                    {
                        game.setCell(i, j, true);
                    }
                }
                
            }
        }
    }
}
