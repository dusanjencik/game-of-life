package semestralniPrace.PR1.dusanjencik.gameoflife;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.geom.Rectangle2D;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Kreslící plátno
 * @author Dušan Jenčík
 */
public class Platno extends JPanel{

    /**
     * Logická stavová hodnota zamezující detekci kláves při ukládání či načítání
     */
    private boolean io = false;
    
    /**
     * Velikost jednoho pole
     */
    private PointF fieldSize;
    
    /**
     * Rozměry herního pole
     */
    private Point dimensions;
    
    /**
     * Logická stavová hodnota povolující vykreslení čísel počtu sousedů do buněk
     */
    private boolean showNumberOfNeighbors = false;
    
    /**
     * Předaná instance třídy Game
     */
    private Game game;
    
    /**
     * Předaná instance třídy GameLoop
     */
    private GameLoop loop;
    
    /**
     * Kreslící plocha zděděná z Canvas
     * @param dimensions rozměry hracího pole
     * @param game instance hry
     * @param loop instance herní smyčky
     */
    public Platno(Point dimensions, Game game, GameLoop loop)
    {
        this.dimensions = dimensions;
        this.game = game;
        this.loop = loop;
        // výpočet velikosti jednotlivých polí
        fieldSize = new PointF((400)/game.getField().x,
                (350)/game.getField().y);
        
        // předání keyboard listeneru
        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(new KeyboardListener());
    }
    
    /**
     * Předání nové herní smyčky
     * @param loop herní smyčka
     */
    public void setLoop(GameLoop loop)
    {
        this.loop = loop;
    }
    
    /**
     * Vykreslovací funkce
     * @param graf Předání parametru graphics
     */
    @Override
    public void paint(Graphics graf)
    {
        Graphics2D g=(Graphics2D) graf;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                             RenderingHints.VALUE_ANTIALIAS_ON);
        Draw d = new Draw(g, dimensions, game.getField(), fieldSize);
        
        g.setColor(Color.BLACK);
        // vybarvení celého plátna
        g.fill(new Rectangle2D.Double(Main.guiSize.x, Main.guiSize.y, 
                Main.guiSize.width, 
                Main.guiSize.height));
        

        g.setColor(Color.WHITE);
        // vykreslení buňek
        d.lives(game.getCells(), showNumberOfNeighbors);
       
        // vykreslení šachovnice
        d.matrix();
        
        // vykreslení ohraničení
        d.border();
        
        // nastavení písma
        if(g.getFont().getName() != "monospaced")
            g.setFont(new Font("monospaced", 0, 12));
        g.setFont(g.getFont().deriveFont(13f));
        
        g.setColor(Color.LIGHT_GRAY);
        
        Object[] obj = {
          loop.getRunning(),
          loop.getPause(),
          loop.getRound(),
          showNumberOfNeighbors
        };
        // vykreslení manuálu
        d.manual(obj);

        g.setFont(g.getFont().deriveFont(11f));
        // vykreslení copyrightu
        d.copyright();
        
        g.setFont(g.getFont().deriveFont(Font.BOLD));
        g.setFont(g.getFont().deriveFont(27f));
        d.gameOfLife();
        
        if(loop.getRound() == 0)
        {
            g.setFont(g.getFont().deriveFont(40f));
            
            d.startSentece();
        }
    }
    
    /**
     * Keyboard listener - odposlouchává stisky kláves
     */
    private class KeyboardListener implements KeyEventDispatcher {
        
        /**
         * Čtení serializovaného Pole
         * @param file název souboru
         * @return Pole
         * @throws Exception Nelze deserializovat
         */
        public Pole readSerializedFile(String file) throws Exception {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
            return (Pole)in.readObject();
        }

        /**
         * Zápis serializovaného Pole
         * @param file název souboru
         * @param pole Pole
         * @throws Exception Nelze serializovat
         */
        public void writeSerializedFile(String file, Pole pole) throws Exception {
            ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(file));
            os.writeObject(pole);
            os.close();
        }
        
        /**
         * Listener
         * @param e argument události
         * @return false
         */
        @Override
        public boolean dispatchKeyEvent(KeyEvent e) {
            if (e.getID() == KeyEvent.KEY_PRESSED) {
                if(!io)
                {
                    if(e.getKeyCode() == KeyEvent.VK_P)
                        loop.setPause(!loop.getPause());
                    if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
                        System.exit(0);
                    if(e.getKeyCode() == KeyEvent.VK_X)
                        loop.step();
                    if(e.getKeyCode() == KeyEvent.VK_N)
                        showNumberOfNeighbors = !showNumberOfNeighbors;
                    if(e.getKeyCode() == KeyEvent.VK_R
                            || e.getKeyCode() == KeyEvent.VK_ENTER)
                    {
                        if(!loop.getRunning())
                        {
                            game.init();
                            game.setTimer(true);
                        }
                        else
                        {
                            game.clear();
                            loop.setRunning(false);
                            game.setTimer(false);
                        }
                    }
                
                    if(e.getKeyCode() == KeyEvent.VK_S)
                    {
                        io = true;
                        JFrame frame = new JFrame();
                        JFileChooser fc = new JFileChooser();
                        fc.setSelectedFile(new File("mapa.txt"));
                        fc.setFileFilter(new FileNameExtensionFilter("Text files", "txt"));
                        fc.showSaveDialog(frame);
                        File selFile = fc.getSelectedFile();

                        try
                        {
                            /*
                            FileWriter fstream = new FileWriter(selFile);
                            BufferedWriter out = new BufferedWriter(fstream);
                            out.write(game.generateField());
                            out.close();*/
                            writeSerializedFile(selFile.toString(), game.getPole());
                            System.out.println("Zapsáno do: "+selFile);
                        }
                        catch (Exception ex){//Catch exception if any
                            System.err.println("Error: " + ex.getMessage());
                        }

                        io = false;
                    }
                    
                    if(e.getKeyCode() == KeyEvent.VK_O)
                    {
                        io = true;
                        
                        JFrame frame = new JFrame();
                        JFileChooser fc = new JFileChooser();
                        fc.setSelectedFile(new File("mapa.txt"));
                        fc.setFileFilter(new FileNameExtensionFilter("Text files", "txt"));
                        fc.showOpenDialog(frame);
                        File selFile = fc.getSelectedFile();

                        try{
                            /*BufferedReader in = new BufferedReader(new FileReader(selFile));
                            System.out.println(selFile);
                            String cele = "";
                            String str = "";
                            //Read File Line By Line
                            while ((str = in.readLine()) != null)   
                                cele += str + "\n";
                            in.close();
                            game.setField(cele);*/
                            game.setPole(readSerializedFile(selFile.toString()));
                            System.out.println("Načteno z: "+selFile);
                        }
                        catch (Exception ex){//Catch exception if any
                            System.err.println("Error: " + ex.getMessage());
                        }
                        

                        io = false;
                    }
                }
                repaint();
            }
            return false;
        }
    }
}
