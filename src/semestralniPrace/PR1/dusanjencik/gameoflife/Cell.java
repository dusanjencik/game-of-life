package semestralniPrace.PR1.dusanjencik.gameoflife;

import java.io.Serializable;

/**
 * Třída pro jednotlivé buňky
 * @author Dušan Jenčík
 */
public class Cell implements Serializable{
    /**
     * Logický stav buňky (false = neživá)
     */
    private boolean state = false;
    
    /**
     * Počet sousedních živých buněk
     */
    private int numberOfNeighbors = 0;
    
    /**
     * Konstruktor buňky
     * @param state stav buňky, jestli je živá nebo ne
     */
    public Cell(boolean state)
    {
        this.state = state;
    }
    
    /**
     * 
     * @return vrátí počet sousedů
     */
    @Override
    public String toString()
    {
        return String.valueOf(numberOfNeighbors);
    }
    
    /**
     * Nastaví počet živých sousedů
     * @param number počet živých sousedů
     */
    public void setNumberOfNeightbors(int number)
    {
        numberOfNeighbors = number;
    }
    
    /**
     * Nastaví stav (false = neživá)
     * @param state logický stav
     */
    public void setState(boolean state)
    {
        this.state = state;
    }
    
    /**
     * Vrátí počet sousedů
     * @return int počet sousedů
     */
    public int getNumberOfNeightbors()
    {
        return numberOfNeighbors;
    }
    
    /**
     * Vrátí stav (false = neživý)
     * @return boolean stav
     */
    public boolean getState()
    {
        return state;
    }
    
    /**
     * Vrátí 0 nebo 1 podle stavu
     * @return logický stav ve dvojkové soustavě
     */
    public String getStateToString()
    {
        return state?"1":"0";
    }
}
