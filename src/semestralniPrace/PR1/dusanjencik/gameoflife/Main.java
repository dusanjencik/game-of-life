package semestralniPrace.PR1.dusanjencik.gameoflife;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import javax.swing.JFrame;

/**
 * Hlavní třída celého programu
 * @author Dušan Jenčík
 */
public class Main {

    /**
     * Rozměry okna
     */
    public static Rectangle guiSize;
    
    /**
     * Hlavní funkce celého programu
     * @param args Program nepotřebuje parametry
     */
    public static void main(String[] args) {
        guiSize = new Rectangle(0, 0, 700, 370);
        
        
        JFrame gui = new JFrame();
        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gui.setSize(guiSize.width, guiSize.height);
        gui.setResizable(false);
        gui.setTitle("Game of Life by Dušan Jenčík");
        
        // Zjištění rozlišení
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        // Vycentrování okna
        int x = (dim.width-guiSize.width)/2;
        int y = (dim.height-guiSize.height)/2;

        // Nastavení parametrů okna
        gui.setLocation(x, y);
        
        // Vytvoření hry
        Game game = new Game(
                new Point(guiSize.x + 10, guiSize.y + 10),
                new Point(40, 40)
                );
        
        gui.add(game.getPlatno());
        gui.setVisible(true);
    }
}
